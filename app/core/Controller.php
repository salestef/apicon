<?php

interface Controller
{
    public function fetch();
}