<?php

/**
 * Configuration for: Database
 */
define('DB_TYPE', 'mysql');
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'api_condor');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_CHARSET', 'utf8');

/**
 * World Cup API resource paths
 */
define('WC_TEAMS_RESOURCE', 'http://worldcup.sfg.io/teams');
define('WC_MATCHES_RESOURCE', 'http://worldcup.sfg.io/matches');
