<?php
/** Require all important and required files used in application. */
require_once 'core/app.php';
require_once 'core/apiController.php';
require_once 'core/model.php';
require_once 'core/db.php';
require_once 'config/config.php';
